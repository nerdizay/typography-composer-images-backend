from fastapi import FastAPI, HTTPException, Body
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field
from pathlib import Path
import os
from typing import List, Annotated
from composer import DESKTOP, create_composition

# uvicorn main:app --reload

app = FastAPI()


origins = [
    "http://localhost:8000",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:5173",
    "http://localhost:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/dirs")
def dirs(path: str | None = None):
    
    current_dir = DESKTOP
    
    if path:
        current_dir = Path(path)
    try:
        parrent_dir = current_dir.parent.absolute()
        res = {
            "parrent_dir": parrent_dir,
            "current_dir": current_dir,
            "dirs": [
                {"name": dir_.name, "type": "file", "path": dir_.absolute()}
                if os.path.isfile(dir_)
                else  {"name": dir_.name, "type": "folder", "path": dir_.absolute()}
                for dir_ in current_dir.iterdir()
            ]
        }
    except NotADirectoryError:
        raise HTTPException(status_code=400, detail="NotADirectoryError")
    else:
        return res
    

class SelectedFolders(BaseModel):
    selected_folders: List[str]
    
@app.post("/composition")
def composition(body: SelectedFolders):
    filepath = create_composition(body.selected_folders)
    if not filepath:
        raise HTTPException(status_code=400, detail="NotADirectoryError")
    return FileResponse(path=filepath, filename='Result.tif', media_type='multipart/form-data')
